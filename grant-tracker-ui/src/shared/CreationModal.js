import React, { Component } from 'react';
import { Modal, Form, Input, Radio, Button, Select, Spin, InputNumber } from 'antd';
import debounce from 'lodash/debounce';


const USER = "User";
const GRANT = "Grant";
const TRANSACTION = "Transaction";

const UserForm = Form.create()(
    class extends React.Component {
        constructor(props) {
            super(props);
            this.lastFetchId = 0;
            this.fetchUser = debounce(this.fetchUser, 800);
            this.state = {
                value: [],
                fetching: false,
                data: []
            };
        }

        fetchUser = (value) => {
            console.log('fetching user', value);
            this.lastFetchId += 1;
            const fetchId = this.lastFetchId;
            this.setState({ data: [], fetching: true });
            fetch('https://randomuser.me/api/?results=5')
                .then(response => response.json())
                .then((body) => {
                    if (fetchId !== this.lastFetchId) { // for fetch callback order
                        return;
                    }
                    const data = body.results.map(user => ({
                        text: `${user.name.first} ${user.name.last}`,
                        value: user.login.username,
                    }));
                    this.setState({ data, fetching: false });
                });
        };

        handleGrantChange = (value) => {
            this.setState({
                value,
                data: [],
                fetching: false,
            });
        };

        onSubmit = (e) => {
            e.preventDefault();
            this.props.form.validateFieldsAndScroll((err, values) => {
                if (!err) {
                    console.log(values);
                    this.props.handleCancel();
                    this.props.form.resetFields();
                }
            });
        };

        render() {
            const { form } = this.props;
            const formItemLayout = {
                labelCol: {
                    xs: { span: 24 },
                    sm: { span: 8 },
                },
                wrapperCol: {
                    xs: { span: 24 },
                    sm: { span: 16 },
                },
            };
            const tailFormItemLayout = {
                wrapperCol: {
                    xs: {
                        span: 24,
                        offset: 0,
                    },
                    sm: {
                        span: 16,
                        offset: 8,
                    },
                },
            };
            const { getFieldDecorator } = form;

            return (
                <Form onSubmit={this.onSubmit}>
                    <Form.Item {...formItemLayout} label={"Name"}>
                        {getFieldDecorator('name', {
                            rules: [{
                                required: true, message: 'Please input your first and last name!'
                            }]
                        })(
                            <Input />
                        )}
                    </Form.Item>
                    <Form.Item
                        {...formItemLayout}
                        label="E-mail"
                    >
                        {getFieldDecorator('email', {
                            rules: [{
                                type: 'email', message: 'The input is not valid E-mail!',
                            }, {
                                required: true, message: 'Please input your e-mail!',
                            }],
                        })(
                            <Input />
                        )}
                    </Form.Item>
                    <Form.Item
                        {...formItemLayout}
                        label="User Type"
                    >
                        {getFieldDecorator('user-type', {
                            rules: [{
                                required: true, message: 'Please select a user type!'
                            }],
                            initialValue: 'admin'
                        })(
                            <Radio.Group>
                                <Radio.Button value="admin">Admin</Radio.Button>
                                <Radio.Button value="user">User</Radio.Button>
                            </Radio.Group>
                        )}
                    </Form.Item>
                    <Form.Item {...formItemLayout} label="Grants">
                        <Select
                            mode="multiple"
                            labelInValue
                            value={this.state.value}
                            placeholder="Apply grants to this user..."
                            notFoundContent={this.state.fetching ? <Spin size="small" /> : null}
                            filterOption={false}
                            onSearch={this.fetchUser}
                            onChange={this.handleGrantChange}
                            style={{ width: '100%' }}
                        >
                            {this.state.data.map(d => <Select.Option key={d.value}>{d.text}</Select.Option>)}
                        </Select>
                    </Form.Item>
                    <Form.Item {...tailFormItemLayout}>
                        <Button type="primary" htmlType="submit">Register</Button>
                    </Form.Item>
                </Form>
            );
        }
    }
);

const GrantForm = Form.create()(
    class extends React.Component {
        constructor(props) {
            super(props);
            this.lastFetchId = 0;
            this.fetchUser = debounce(this.fetchUser, 800);
            this.state = {
                value: [],
                fetching: false,
                data: []
            };
        }

        fetchUser = (value) => {
            console.log('fetching user', value);
            this.lastFetchId += 1;
            const fetchId = this.lastFetchId;
            this.setState({ data: [], fetching: true });
            fetch('https://randomuser.me/api/?results=5')
                .then(response => response.json())
                .then((body) => {
                    if (fetchId !== this.lastFetchId) { // for fetch callback order
                        return;
                    }
                    const data = body.results.map(user => ({
                        text: `${user.name.first} ${user.name.last}`,
                        value: user.login.username,
                    }));
                    this.setState({ data, fetching: false });
                });
        };

        handleGrantChange = (value) => {
            this.setState({
                value,
                data: [],
                fetching: false,
            });
        };

        onSubmit = (e) => {
            e.preventDefault();
            this.props.form.validateFieldsAndScroll((err, values) => {
                if (!err) {
                    console.log(values);
                    this.props.handleCancel();
                    this.props.form.resetFields();
                }
            });
        };

        render() {
            const { form } = this.props;
            const formItemLayout = {
                labelCol: {
                    xs: { span: 24 },
                    sm: { span: 8 },
                },
                wrapperCol: {
                    xs: { span: 24 },
                    sm: { span: 16 },
                },
            };
            const tailFormItemLayout = {
                wrapperCol: {
                    xs: {
                        span: 24,
                        offset: 0,
                    },
                    sm: {
                        span: 16,
                        offset: 8,
                    },
                },
            };
            const { getFieldDecorator } = form;

            return (
                <Form onSubmit={this.onSubmit}>
                    <Form.Item {...formItemLayout} label={"Grant Name"}>
                        {getFieldDecorator('name', {
                            rules: [{
                                required: true, message: 'Please input a name identifier for the grant!'
                            }]
                        })(
                            <Input />
                        )}
                    </Form.Item>
                    <Form.Item
                        {...formItemLayout}
                        label="Amount"
                    >
                        {getFieldDecorator('email', {
                            rules: [ {
                                required: true, message: 'Please enter an amount for the grant.',
                            }],
                        })(
                            <InputNumber
                                defaultValue={1000}
                                formatter={value => `$ ${value}`.replace(/\B(?=(\d{3})+(?!\d))/g, ',')}
                                parser={value => value.replace(/\$\s?|(,*)/g, '')}
                            />
                        )}
                    </Form.Item>
                    <Form.Item {...formItemLayout} label="Users">
                        <Select
                            mode="multiple"
                            labelInValue
                            value={this.state.value}
                            placeholder="Apply users to this grant..."
                            notFoundContent={this.state.fetching ? <Spin size="small" /> : null}
                            filterOption={false}
                            onSearch={this.fetchUser}
                            onChange={this.handleGrantChange}
                            style={{ width: '100%' }}
                        >
                            {this.state.data.map(d => <Select.Option key={d.value}>{d.text}</Select.Option>)}
                        </Select>
                    </Form.Item>
                    <Form.Item {...tailFormItemLayout}>
                        <Button type="primary" htmlType="submit">Register</Button>
                    </Form.Item>
                </Form>
            );
        }
    }
);

const TransactionForm = Form.create()(
    class extends React.Component {
        constructor(props) {
            super(props);
            this.lastFetchId = 0;
            this.fetchUser = debounce(this.fetchUser, 800);
            this.state = {
                value: [],
                fetching: false,
                data: []
            };
        }

        fetchUser = (value) => {
            console.log('fetching user', value);
            this.lastFetchId += 1;
            const fetchId = this.lastFetchId;
            this.setState({ data: [], fetching: true });
            fetch('https://randomuser.me/api/?results=5')
                .then(response => response.json())
                .then((body) => {
                    if (fetchId !== this.lastFetchId) { // for fetch callback order
                        return;
                    }
                    const data = body.results.map(user => ({
                        text: `${user.name.first} ${user.name.last}`,
                        value: user.login.username,
                    }));
                    this.setState({ data, fetching: false });
                });
        };

        handleGrantChange = (value) => {
            this.setState({
                value,
                data: [],
                fetching: false,
            });
        };

        onSubmit = (e) => {
            e.preventDefault();
            this.props.form.validateFieldsAndScroll((err, values) => {
                if (!err) {
                    console.log(values);
                    this.props.handleCancel();
                    this.props.form.resetFields();
                }
            });
        };

        render() {
            const { form } = this.props;
            const formItemLayout = {
                labelCol: {
                    xs: { span: 24 },
                    sm: { span: 8 },
                },
                wrapperCol: {
                    xs: { span: 24 },
                    sm: { span: 16 },
                },
            };
            const tailFormItemLayout = {
                wrapperCol: {
                    xs: {
                        span: 24,
                        offset: 0,
                    },
                    sm: {
                        span: 16,
                        offset: 8,
                    },
                },
            };
            const { getFieldDecorator } = form;

            return (
                <Form onSubmit={this.onSubmit}>
                    <Form.Item {...formItemLayout} label={"Name"}>
                        {getFieldDecorator('name', {
                            rules: [{
                                required: true, message: 'Please input your first and last name!'
                            }]
                        })(
                            <Input />
                        )}
                    </Form.Item>
                    <Form.Item
                        {...formItemLayout}
                        label="E-mail"
                    >
                        {getFieldDecorator('email', {
                            rules: [{
                                type: 'email', message: 'The input is not valid E-mail!',
                            }, {
                                required: true, message: 'Please input your e-mail!',
                            }],
                        })(
                            <Input />
                        )}
                    </Form.Item>
                    <Form.Item
                        {...formItemLayout}
                        label="User Type"
                    >
                        {getFieldDecorator('user-type', {
                            rules: [{
                                required: true, message: 'Please select a user type!'
                            }],
                            initialValue: 'admin'
                        })(
                            <Radio.Group>
                                <Radio.Button value="admin">Admin</Radio.Button>
                                <Radio.Button value="user">User</Radio.Button>
                            </Radio.Group>
                        )}
                    </Form.Item>
                    <Form.Item {...formItemLayout} label="Grants">
                        <Select
                            mode="multiple"
                            labelInValue
                            value={this.state.value}
                            placeholder="Apply grants to this user..."
                            notFoundContent={this.state.fetching ? <Spin size="small" /> : null}
                            filterOption={false}
                            onSearch={this.fetchUser}
                            onChange={this.handleGrantChange}
                            style={{ width: '100%' }}
                        >
                            {this.state.data.map(d => <Select.Option key={d.value}>{d.text}</Select.Option>)}
                        </Select>
                    </Form.Item>
                    <Form.Item {...tailFormItemLayout}>
                        <Button type="primary" htmlType="submit">Register</Button>
                    </Form.Item>
                </Form>
            );
        }
    }
);

class CreationModal extends Component {

    render() {
        let form = null;
        switch(this.props.type) {
            case USER:
                form = <UserForm handleCancel={this.props.handleCancel} />;
                break;
            case GRANT:
                form = <GrantForm handleCancel={this.props.handleCancel} />;
                break;
            case TRANSACTION:
                form = <TransactionForm handleCancel={this.props.handleCancel} />;
                break;
            default:
        }

        return (
            <Modal
                title={"Create New " + this.props.type}
                visible={this.props.visible}
                onCancel={this.props.handleCancel}
                footer={false}
            >
                {form}
            </Modal>
        )
    }
}

export default CreationModal;