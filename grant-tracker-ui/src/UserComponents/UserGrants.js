import React, { Component } from 'react';
import { Table, Card, Row, Col } from 'antd';
import { LineChart, Line, CartesianGrid, XAxis, YAxis, ResponsiveContainer, PieChart, Pie, Tooltip, Cell } from 'recharts';

import './UserGrants.css';

const columns = [{
    title: 'Description',
    dataIndex: 'description'
}, {
    title: 'Amount',
    dataIndex: 'amount',
    defaultSortOrder: 'descend',
    sorter: (a, b) => a.amount - b.amount,
}, {
    title: 'Date',
    dataIndex: 'date',
    sorter: (a, b) => Date.parse(a.date) - Date.parse(b.date),
}];
const data = [[{
    key: '1',
    description: 'New supplies',
    amount: 32.52,
    date: '2018-02-01',
}, {
    key: '2',
    description: 'Fix something',
    amount: 4201.32,
    date: '2018-01-30',
}, {
    key: '3',
    description: 'Maintenance Fees',
    amount: 20.00,
    date: '2017-05-21',
}],
    [{
        key: '1',
        description: 'Expenses',
        amount: 132.52,
        date: '2018-06-12',
    }, {
        key: '2',
        description: 'Salary',
        amount: 401.32,
        date: '2018-02-21',
    }]
];
const grantData = [[{ date: "2018-04-10", uv: 100.00 }, {date: "2018-04-11", uv: 88.32 }, { date: "2018-04-12", uv: 53.12 }, { date: "2018-04-13", uv: 50.00 }, { date: "2018-04-14", uv: 21.23 } ],
[{ date: "2018-04-10", uv: 250.00 }, {date: "2018-04-11", uv: 188.32 }, { date: "2018-04-12", uv: 153.12 }, { date: "2018-04-13", uv: 150.00 }, { date: "2018-04-14", uv: 21.23 }]];
const allocationData = [[{"name": "Used", "value": 79}, {"name": "Free", "value": 21}], [{"name": "Used", "value": 92}, {"name": "Free", "value": 8}]];

class UserGrants extends Component {
    state = {
        grants: [
            {
                "name": "Sample Grant",
                "date_granted": "December 12, 2017",
                "balance": 423.12
            },
            {
                "name": "Another Grant",
                "date_granted": "January 12, 2018",
                "balance": 12.43
            }
        ],
        grantSelectedIndex: 0
    };

    changeActiveGrant = (grantSelectedIndex) => {
        this.setState({grantSelectedIndex});
    };

    render() {

        let grantList = this.state.grants.map((grant, grantIndex) => {
            return (
                <Col key={grantIndex} lg={8} md={24} sm={24} xs={24}>
                    <Card onClick={() => this.changeActiveGrant(grantIndex)}
                          className={grantIndex === this.state.grantSelectedIndex ? "grant-card grant-active" : "grant-card"}>
                        <Col md={18} sm={12} xs={12}>
                            <h4 className="grant-title">{grant.name}</h4>
                            <span className="grant-balance">${grant.balance}</span>
                            {grantIndex === this.state.grantSelectedIndex ? <span className="balance-description">Balance Remaining</span> : null}
                            <br/>
                            <small>Date Granted: {grant.date_granted}</small>
                        </Col>
                        <Col md={6} sm={12} xs={12}>
                            <ResponsiveContainer height={100} width="100%" className="small-balance-graph">
                                <PieChart>
                                    <Pie data={allocationData[grantIndex]} dataKey="value" nameKey="name" cx="50%" cy="50%"
                                         innerRadius={25} outerRadius={35} paddingAngle={5}>
                                        <Cell fill="red" />
                                        <Cell fill="green" />
                                    </Pie>
                                    <Tooltip />
                                </PieChart>
                            </ResponsiveContainer>
                        </Col>
                    </Card>
                </Col>
            );
        });

        return (
            <div>
                <h1>My Grants</h1>
                <Row className="grant-list" gutter={8}>
                    {grantList}
                </Row>

                <hr />

                <Row gutter={16}>
                    <Col xs={24} sm={24} md={18}>
                        <Card title="Balance Over Time">
                            <ResponsiveContainer height={200} width="100%">
                                <LineChart data={grantData[this.state.grantSelectedIndex]}>
                                    <Line type="monotone" dataKey="uv" stroke="#0085ad" />
                                    <CartesianGrid stroke="#ccc"/>
                                    <XAxis dataKey="date" padding={{ left: 20, right: 20 }} height={30}/>
                                    <YAxis unit="$" padding={{ right: 20 }}/>
                                </LineChart>
                            </ResponsiveContainer>
                        </Card>
                    </Col>
                    <Col span={6} xs={24} sm={24} md={6}>
                        <Card title="Funds Allocation">
                            <ResponsiveContainer height={200} width="100%">
                                <PieChart>
                                    <Pie data={allocationData[this.state.grantSelectedIndex]} dataKey="value" nameKey="name" cx="50%" cy="50%"
                                         innerRadius={60} outerRadius={80} paddingAngle={5} label labelLine={false}>
                                        <Cell fill="red" />
                                        <Cell fill="green" />
                                    </Pie>
                                    <Tooltip />
                                </PieChart>
                            </ResponsiveContainer>
                        </Card>
                    </Col>
                </Row>
                <h2 className="transaction-header">Transaction History</h2>
                <Table columns={columns} dataSource={data[this.state.grantSelectedIndex]} />
            </div>
        );
    }
}

export default UserGrants;