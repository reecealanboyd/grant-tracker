import React, {Component} from 'react';
import {Layout, Menu, Icon} from 'antd';
import { Button, Form, Grid, Image, Message, Segment, Menu as SemanticMenu } from 'semantic-ui-react';
import Axios from 'axios';
import jwt_decode from 'jwt-decode';

import './App.css';
import UserContent from "./UserComponents/UserContent";
import AdminContent from './AdminComponents/AdminContent';
// import Login from './Components/login';
// import Navbar from './Components/navbar';


const { Header, Sider, Footer, Content } = Layout;
const ADMIN = "admin";
const USER = "user";
const CREATE = "create";

const MY_GRANTS = "my-grants";

class App extends Component {
    state = {
        loggedIn: ADMIN,
        viewActive: CREATE,
        password: '',
        username: '',
        access_token: '',
        test: ''
    };

    debugToggleLogin = () => {
        if (this.state.loggedIn === ADMIN) {
            this.setState({
                loggedIn: USER
            });
        } else {
            this.setState({
                loggedIn: ADMIN
            });
        }
    };

    determineUserType = () => {
        if (this.state.username === 'user' && this.state.password === 'userpass') {
            this.state.loggedIn = USER;
        }
        else if (this.state.username === 'admin' && this.state.password === 'adminpass') {
            this.state.loggedIn = ADMIN;
        }
    }

    authenticateUser = (e) => {
        e.preventDefault();
        const user = [
            {username : this.state.username},
            {password : this.state.password}
        ]

          Axios.post('https://he9ah89p3j.execute-api.us-east-2.amazonaws.com/dev/login', {}, {
            auth: {
              username: this.state.username,
              password: this.state.password
            }
          }).then(function(response) {
            console.log('Authenticated');
            // console.log(response);
            this.state.access_token = response.data.access_token;
            localStorage.setItem("access_token", this.state.access_token);
            this.state.test = this.parseJwt(this.state.access_token);
            console.log(test);
          }).catch(function(err){

          });


        // fetch('/api/form-submit-url', {
        //     method: 'POST',
        //     body: data,
        // // });
    }

    parseJwt = (token) => {
        try {
          return JSON.parse(atob(token.split('.')[1]));
        } catch (e) {
          return null;
        }
      };
      

    handleUsernameChange = (e) => {
        this.setState({username: e.target.value})
        console.log("Username "+e.target.value);
    }

    handlePasswordChange = (e) => {
        this.setState({password: e.target.value})
        console.log("Password "+e.target.value);
    }

    // handleLogin = (userType) => {
    //     if (userType === ADMIN) {
    //         this.state.loggedIn === ADMIN;
    //     }
    //     else if (userType === USER) {
    //         this.state.loggedIn === USER;
    //     }
    //     else {
    //         this.state.loggedIn === "";
    //     }
    // };

    swapView = (event) => {
        this.setState({
            viewActive: event.key
        });
    };

    render() {
        const USER_MENU = (
            <Menu theme="dark" mode="inline" defaultSelectedKeys={['my-grants']}>
                <Menu.Item key={MY_GRANTS}>
                    <Icon type="user" />
                    <span className="nav-text">My Grants</span>
                </Menu.Item>
            </Menu>
        );

        const ADMIN_MENU = (
            <Menu onClick={(e) => this.swapView(e)} theme="dark" mode="inline" defaultSelectedKeys={[this.state.viewActive]}>
                <Menu.Item key={CREATE}>
                    <Icon type="user" />
                    <span className="nav-text">Create</span>
                </Menu.Item>
            </Menu>
        );

        const SIDER = (
            <Sider
                        breakpoint="lg"
                        collapsedWidth="0"
                    >
                        <div className="logo" />
                        { this.state.loggedIn === USER ? USER_MENU : ADMIN_MENU }
                    </Sider>    
        );

        const LOGGED_IN_HEADER = (
            <Header style={{ background: '#fff', padding: 0 }}>
                            <img onClick={this.debugToggleLogin} className="metis-logo" alt="Metis Logo" src={"images/metis-icon.png"} />
                            <span className="user-text">{this.state.loggedIn === USER ? "Sample User" : "Administrator"}</span>
                            <img className="user-image" src={this.state.loggedIn === USER ? "user-icon.png" : "admin-icon.png"} alt="User Image" />
                        </Header>
        )

        const LOGGED_OUT_HEADER = (
            // <Header style={{ background: '#fff', padding: 0 }}>
            //                 <img onClick={this.debugToggleLogin} className="metis-logo" alt="Metis Logo" src={"images/metis-icon.png"} />
            //                 {/* <span className="user-text">{this.state.loggedIn === USER ? "Sample User" : "Administrator"}</span>
            //                 <img className="user-image" src={this.state.loggedIn === USER ? "user-icon.png" : "admin-icon.png"} alt="User Image" /> */}
            //                 <Button position="right" color="green">Sign In</Button>
            //             </Header>
            <SemanticMenu size="mini">
                <SemanticMenu.Item name='browse'>
                    <Image onClick={this.debugToggleLogin} size="mini" src="images/metis-icon.png"/>
                </SemanticMenu.Item>
                <SemanticMenu.Menu position='right'>
              </SemanticMenu.Menu>
            </SemanticMenu>
        );

        const LOGIN = (
            <div className='login-form'>
                <br/>
                <br/>

                <Grid
                    textAlign='center'
                    style={{ height: '100%' }}
                    verticalAlign='top'
                >
                    <Grid.Column style={{ maxWidth: 450 }}>
                        <Form size='large' onSubmit={this.authenticateUser}>
                            <Segment >
                                <Image src='images/metis-logo.png'/>
                                <br/>
                                <br/>
                                <h2>Log-in to your account</h2>
                                <Form.Input
                                    fluid
                                    icon='user'
                                    iconPosition='left'
                                    placeholder='Username'
                                    name="username"
                                    onChange={this.handleUsernameChange}
                                />
                                <Form.Input
                                    fluid
                                    icon='lock'
                                    iconPosition='left'
                                    placeholder='Password'
                                    type='password'
                                    name="password"
                                    onChange={this.handlePasswordChange}

                                />
                                <Form.Button fluid size='large' color='blue' > Submit </Form.Button>
                            </Segment>
                        </Form>
                    </Grid.Column>
                </Grid>
            </div>
        )

        return (
            <div>
                <Layout>
                { this.state.loggedIn === USER ? <div></div> : SIDER }
                    <Layout>
                    { this.state.loggedIn === USER ? LOGGED_OUT_HEADER : LOGGED_IN_HEADER }
                        <Content style={{ margin: '24px 16px 0' }}>
                            <div style={{ padding: 24, background: '#fff', minHeight: 360 }}>
                                { this.state.loggedIn === USER ? LOGIN : <AdminContent view={this.state.viewActive} /> }
                            </div>
                        </Content>
                        <Footer style={{ textAlign: 'center' }}>
                            Metis Foundation ©2018 Created by AiQ Engineering
                        </Footer>
                    </Layout>
                </Layout>
            </div>
        );
    }
}

export default App;