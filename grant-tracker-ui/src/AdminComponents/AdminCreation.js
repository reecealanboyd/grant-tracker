import React, { Component } from 'react';
import { Row, Col, Button, Modal } from 'antd';

import './AdminCreation.css';
import CreationModal from "../shared/CreationModal";

const USER = "User";
const GRANT = "Grant";
const TRANSACTION = "Transaction";

class AdminCreation extends Component {
    state = {
        createUserVisible: false,
        createGrantVisible: false,
        createTransactionVisible: false
    };

    showModal = (modal) => {
        switch (modal) {
            case USER:
                this.setState({ createUserVisible: true });
                break;
            case GRANT:
                this.setState({ createGrantVisible: true });
                break;
            case TRANSACTION:
                this.setState({ createTransactionVisible: true });
                break;
            default:
        }
    };

    handleCancel = () => {
        this.setState({
            createUserVisible: false,
            createGrantVisible: false,
            createTransactionVisible: false
        });
    };

    render() {
        return (
            <div>
                <h1>Create Records</h1>
                <p>Use this panel to create users, grants, and transactions.</p>
                <Row className="create-button-panel" gutter={10}>
                    <Col md={8} xs={24}>
                        <Button className="create-button blue-bg" type="primary" shape="circle" onClick={() => this.showModal(USER)} icon="user" />
                        <p className="button-caption">Users</p>
                        <CreationModal wrappedComponentRef={this.saveFormRef} visible={this.state.createUserVisible} handleCancel={this.handleCancel} type={USER}/>
                    </Col>
                    <Col md={8} xs={24}>
                        <Button className="create-button blue-bg" type="primary" shape="circle" onClick={() => this.showModal(GRANT)} icon="solution" />
                        <p className="button-caption">Grants</p>
                        <CreationModal visible={this.state.createGrantVisible} handleCancel={this.handleCancel} type={GRANT}/>
                    </Col>
                    <Col md={8} xs={24}>
                        <Button className="create-button blue-bg" type="primary" shape="circle" onClick={() => this.showModal(TRANSACTION)} icon="shopping-cart" />
                        <p className="button-caption">Transactions</p>
                        <CreationModal visible={this.state.createTransactionVisible} handleCancel={this.handleCancel} type={TRANSACTION}/>
                    </Col>
                </Row>
            </div>
        );
    }
}

export default AdminCreation;