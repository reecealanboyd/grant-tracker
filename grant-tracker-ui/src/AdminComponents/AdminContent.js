import React, { Component } from 'react';
import AdminCreation from './AdminCreation';

const CREATE = "create";

class AdminContent extends Component {
    constructor(props) {
        super(props);
        this.state = {
            visible: this.props.view
        };
    };

    componentWillReceiveProps(nextProps) {
        this.setState({
            visible: nextProps.view
        });
    }

    render() {
        let content = null;
        switch(this.state.visible) {
            case CREATE:
                content = <AdminCreation />;
                break;
            default:
                content = <AdminCreation />;
        }

        return (
            <div>{content}</div>
        );
    }
}

export default AdminContent;