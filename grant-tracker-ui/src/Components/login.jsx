import React, {Component} from 'react'
import { Icon, Button, Form, Grid, Header, Image, Message, Segment } from 'semantic-ui-react'

export class Login extends Component {
    render() {
        return (
            <div className='login-form'>
                <br/>
                <br/>

                <Grid
                    textAlign='center'
                    style={{ height: '100%' }}
                    verticalAlign='top'
                >
                    <Grid.Column style={{ maxWidth: 450 }}>
                        <Form size='large'>
                            <Segment >
                                <Image src='images/metis-logo.png'/>
                                <br/>
                                <br/>
                                <Header as='h2' color='grey' textAlign='center'> Log-in to your account</Header>
                                <Form.Input
                                    fluid
                                    icon='user'
                                    iconPosition='left'
                                    placeholder='E-mail address'
                                />
                                <Form.Input
                                    fluid
                                    icon='lock'
                                    iconPosition='left'
                                    placeholder='Password'
                                    type='password'
                                />
                                <Button fluid size='large' color='blue'> Submit </Button>
                            </Segment>
                        </Form>
                    </Grid.Column>
                </Grid>
            </div>
        )
    }
}

export default Login;