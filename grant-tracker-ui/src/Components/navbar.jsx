import React,{Component} from 'react';
import {Image, Menu,Button} from 'semantic-ui-react';
export class NavBar extends Component {
    constructor(props){
        super(props);
        this.state = { 
            activeItem: 'home',
            isLoggedIn: false
        }
    }
    handleItemClick = (e, { name }) => this.setState({ activeItem: name });

    loggedIn(){
        this.setState({isLoggedIn: true});
    }
    loggedOut(){
         this.setState({isLoggedIn: false});
    }
    render() {
        let button;
        const { activeItem,isLoggedIn } = this.state;

        if(isLoggedIn){
            button = <Button onClick={this.toggleLogin}color="red">Sign Out</Button>
        }else{
            button = <Button onClick={this.toggleLogin}color="green">Sign In</Button>
        }
        return (
            <Menu size="mini">
                <Menu.Item name='browse' active={activeItem === 'browse'} onClick={this.handleItemClick}>
                    <Image size="mini" src="images/metis-icon.png"/>
                </Menu.Item>
                <Menu.Menu position='right'>
                    <Menu.Item name='signup' active={activeItem === 'signup'} onClick={this.handleItemClick}>
                    {button}
                    </Menu.Item>
              </Menu.Menu>
            </Menu>
          )
        }
}export default NavBar;